const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2) || typeof str1 === "object" || typeof str2 === "object" || typeof str1 === "number" || typeof str2 === "number") { return false; }
  str1 = str1.trim() === "" ? 0 : parseFloat(str1);
  str2 = str2.trim() === "" ? 0 : parseFloat(str2);
  return `${str1 + str2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = listOfPosts.filter(post => post.author === authorName).length;
  let comments = listOfPosts.reduce((quantityOfComments, post) => {
    return quantityOfComments + (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length);
  }, 0);
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  const queu = people.map(e => +e);
  let sum = 0;
  for (const i of queu){
    if (i === 25){
      sum+=i
    } else{
      sum = sum - (i - 25);
    }
    if (sum < 0){
      return 'NO'
    }
    sum+=25
  }
  if (sum){
    return 'YES';
  }
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
